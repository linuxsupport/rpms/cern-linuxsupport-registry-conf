Name: cern-linuxsupport-registry-conf
Version: 1.0
Release: 1%{?dist}
Summary: provide aliases for CERN Linux Support containers
Group: Applications/System
Source: %{name}-%{version}.tgz
License: GPL
Vendor: CERN
Packager: Linux.Support@cern.ch
URL: http://cern.ch/linux/

BuildRoot: %{_tmppath}/%{name}-root
BuildArch: noarch

Requires: containers-common

%description
%{name} provides aliases for CERN Linux Support
container images.

%prep
%setup -q

%build

%install
install -d %{buildroot}/%{_sysconfdir}/containers/registries.conf.d/
install -p -m 0444 001-linuxsupport.conf %{buildroot}/%{_sysconfdir}/containers/registries.conf.d/001-linuxsupport.conf

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%config %{_sysconfdir}/containers/registries.conf.d/001-linuxsupport.conf

%changelog
* Wed Jun 21 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.0-1
- initial release
